(function (window, document) {
  'use strict';

  const options = {
    includeScore: true,
    keys: ['id', 'title', 'excerpt', 'tags']
  }

  var json;
  var fuse;
  var items;
  var pagingResult = {};
  var pageNumber;
  var pageSize = 10;
  var postsEl = document.getElementById("posts");
  var alertModal = new bootstrap.Modal(document.getElementById('alert'), {
    keyboard: false
  });
  var alertModalMessage = document.getElementById('alert-message');
  var searchInfo = document.getElementById('search-info');


  function search(keyword) {

    searchInfo.style.display = 'block';
    searchInfo.innerHTML = `<span>Kata kunci : ${keyword}</span>`;

    if (keyword.trim().length < 3) {
      postsEl.innerHTML = `
      <div class="alert alert-light border" role="alert">
        Kata kunci minimal 3 huruf
      </div>
      `;

      alertModalMessage.innerText = 'Kata kunci minimal 3 huruf';
      alertModal.toggle();
      return;
    }

    if (!json) {
      fetch('/posts.json').then((response) =>
        response.json().then((resp) => {
          json = resp;

          const index = Fuse.createIndex(options.keys, json);
          fuse = new Fuse(json, options, index);

          searchFuse(keyword);
        })
      )
    } else {
      searchFuse(keyword);
    }
  };

  function searchFuse(keyword) {
    items = fuse.search(keyword);

    pageNumber = 1;
    paging();
    showPosts();
  }

  function paging() {
    const offset = pageSize * (pageNumber - 1);
    const pageCount = Math.ceil(items.length / pageSize);

    pagingResult.items = items.slice(offset, pageSize * pageNumber);
    pagingResult.previous = (pageNumber == 1) ? "" : "/" + (parseInt(pageNumber) - 1) + "/";
    pagingResult.next = (pageCount > pageNumber) ? "/" + (parseInt(pageNumber) + 1) + "/" : "";
    pagingResult.pageNumber = pageNumber;
    pagingResult.pageCount = pageCount;
    pagingResult.next = next;
    pagingResult.previous = previous;

    window.paging(pagingResult);
  };

  function showPosts() {
    if (items && items.length > 0) {
      getPostsHtml();
    } else {
      postsEl.innerHTML = `
      <div class="alert alert-light border" role="alert">
        Data tidak ditemukan
      </div>
      `;
    }
  }

  function getPostsHtml() {
    postsEl.innerHTML = '';

    for (var i = 0; i < pagingResult.items.length; i++) {

      var item = pagingResult.items[i].item;
      var tags = '';

      for (var j = 0; j < item.tags.length; j++) {
        tags += `<a href="/tags/${item.tags[j].toLowerCase()}"><span class="icon-tag"></span><span>${item.tags[j]}</span></a>`
      }

      postsEl.innerHTML += `
      <div class="mb-3 text-decoration-none link-dark">
        <h3 class="m-0"><a href="${item.url}" class="link-secondary">${item.title}</a></h3>
        <div class="d-flex align-items-center post-meta">
          <span class="icon-calendar"></span><span class="me-3">${dateToString(item.date)}</span>
          <span class="icon-author"></span><a href="/authors/${item.author.username}">${item.author.displayName}</a>
        </div>
        ${item.excerpt}
        <div class="tags">${tags}</div>
      </div>
      `
    }
  }

  function next() {
    if (pagingResult.pageNumber < pagingResult.pageCount) {
      pageNumber++;
      paging();
      showPosts();
    }
  }

  function previous() {
    if (pagingResult.pageNumber > 1) {
      pageNumber--;
      paging();
      showPosts();
    }
  }

  function dateToString(str){
    var date = new Date(str);
    return  n(date.getUTCDate()) + '-' + n(date.getUTCMonth() + 1) + '-' + (date.getUTCFullYear());
  }

  function n(n){
    return n > 9 ? "" + n: "0" + n;
  }
  
  window.search = search;

})(window, document);