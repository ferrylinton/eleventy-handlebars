(function (window, document) {
  'use strict';

  document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
      initAksara();
      initSearch();
      initPaging();

      var navbarIcons = document.getElementById('navbar-icons').getElementsByTagName('a');
      for (var i = 0; i < navbarIcons.length; i++) {
          console.log(navbarIcons[i].pathname); 
          
          if(navbarIcons[i].pathname === window.location.pathname || navbarIcons[i].pathname + '/' === window.location.pathname){
            navbarIcons[i].classList.add('active');
          }
      }
    }
  }

  document.addEventListener("DOMContentLoaded", function(){
    window.addEventListener('scroll', function() {
        if (window.scrollY > 50) {
          let navbarEl = document.getElementById('navbar');
          navbarEl.style.borderBottom = "solid 1px #e9e3e3"; 
          navbarEl.style.position = "fixed";
          navbarEl.style.top = "0";
          navbarEl.style.boxShadow = "0 4px 28px -11px rgba(69,77,89,.30)";
          document.body.style.paddingTop = navbarEl.offsetHeight + 'px';
        } else {
          let navbarEl = document.getElementById('navbar');
          navbarEl.style.borderBottom = "none"; 
          navbarEl.style.position = "static";
          navbarEl.style.top = "0";
          navbarEl.style.boxShadow = "none";
          document.body.style.paddingTop = '0';
        } 
    });
  }); 

  function initAksara() {
    window.convertToAksaraBatak(document.getElementById('to-aksara'));
  }

  function initSearch() {
    var search = document.getElementById('search');
    search.onkeydown = function (e) {
      if (e.key === 'Enter') {
        location.href = "/search/?keyword=" + e.target.value;
      }
    }

    var btnSearch = document.getElementById('btn-search');
    btnSearch.onclick = function () {
      location.href = "/search/?keyword=" + search.value;
    }

    if(location.pathname.includes('/search/') ){
      var queryString = window.location.search;
      var urlParams = new URLSearchParams(queryString);
      var keyword = urlParams.get('keyword');
      search.value = keyword;
      window.search(keyword);
    }
  }

  function initPaging(){
    var pagingInfo = document.getElementById('paging-info');

    if(pagingInfo){
      var obj = JSON.parse('{' + pagingInfo.textContent + '}');
      window.paging(obj);    
    }
  }

})(window, document);
