(function (window, document) {
    'use strict';

    function paging(obj) {
        var paging = document.getElementById('paging');
        var pagingPrevious = document.getElementById('paging-previous');
        var pagingNext = document.getElementById('paging-next');
        var pagingText = document.getElementById('paging-text');

        if (obj.pageCount > 1) {
            paging.style.display = 'flex';
            pagingText.innerText = 'Halaman ' + obj.pageNumber + ' dari ' + obj.pageCount;

            if (obj.previous == '') {
                pagingPrevious.classList.add("disabled");
                pagingPrevious.onclick = function (event) {
                    event.preventDefault();
                }
            } else {
                if(obj.pageNumber == 1){
                    pagingPrevious.classList.add("disabled");
                }else{
                    pagingPrevious.classList.remove('disabled');
                }
                
                if(obj.items){
                    pagingPrevious.onclick = function (event) {
                        event.preventDefault();
                        obj.previous();
                    }
                }else{
                    pagingPrevious.href = obj.previous;
                }
                
            }

            if (obj.next == '') {
                pagingNext.classList.add("disabled");
                pagingNext.onclick = function (event) {
                    event.preventDefault();
                }
            } else {
                if (obj.pageNumber < obj.pageCount) {
                    pagingNext.classList.remove('disabled');
                }else{
                    pagingNext.classList.add("disabled");
                }

                if(obj.items){
                    pagingNext.onclick = function (event) {
                        event.preventDefault();
                        obj.next();
                    }
                }else{
                    pagingNext.href = obj.next;
                }
            }
        }else{
            paging.style.display = "none";
        }
    }

    window.paging = paging;

})(window, document);