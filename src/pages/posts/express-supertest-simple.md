---
layout: post
title: "Contoh Sederhana Express dan Supertest"
tags: 
 - Expressjs
 - Mocha
 - Supertest
date: 2022-01-20
author: 
  displayName: Ferry L. H.
  username: ferrylinton
svgs: 
  - node-svg
  - sequelize-color-svg
---

<!-- Excerpt Start -->

Di postingan berikut, saya akan membuat aplikasi **[Node.js](https://nodejs.dev/)** sederhana dengan menggunakan **[Express](https://expressjs.com/)**,  dan setelah itu saya akan membuat tes integrasi sederhana dengan menggunakan **[Supertest](https://github.com/visionmedia/supertest)** dan **[Mocha](https://mochajs.org/)**

<!-- Excerpt End -->

**[Express](https://expressjs.com/)** adalah platform untuk mengembangkan aplikasi web Node.js yang minimal dan fleksibel yang menyediakan sekumpulan fitur yang kokoh (mempunyai kemampuan untuk mengatasi kesalahan selama eksekusi) untuk aplikasi web dan seluler.

**[Mocha](https://mochajs.org/)** adalah platform pengujian JavaScript yang banyak fitur yang berjalan di Node.js dan di browser, membuat pengujian asinkron menjadi sederhana dan menyenangkan.

**[Supertest](https://github.com/visionmedia/supertest)** adalah librari untuk menguji server HTTP.

## Langkah-Langkah Yang Digunakan

### 1. Buat proyek **[Node.js](https://nodejs.dev/)**

```
npm init -y
```

### 2. Librari yang digunakan

- [Mocha](https://mochajs.org/)  
  Instal dengan npm secara **global**  
  > npm install --global mocha

- [Supertest](https://www.npmjs.com/package/supertest)  
  Instal dengan npm sebagai **development dependency** untuk proyek  
  > npm install supertest --save-dev

- [Express](https://expressjs.com/)  
  Instal dengan npm sebagai **dependency** untuk proyek  
  > npm install express --save  


### 3. Struktur proyek yang akan digunakan

```
.
├── index.js
├── package.json
├── README.md
├── src
│   ├── app.js
│   └── routes
│       └── simple-route.js
└── test
    └── routes
        └── simple-route.spec.js
```

### 4. Buat berkas **simple-route.js**

Tulis kode berikut

```js
const express = require('express');

// Ada dua cara Routing di Express
// 1. Routing dengan menggunakan app.route() (app adalah instance dari express)
// 2. Routing dengan dengan menggunakan express.Router()

// Instance dari express.Router()
const router = express.Router();

// Mendaftarkan path '/' dengan handler
router.get('/', getData);

// Handler yang akan dipangggil saat memanggil path '/'
function getData(req, res) {
    let data = {
        name: 'Ferry L. H.'
    }

    res.status(200).json(data);
}

module.exports = router;
```

### 5. Buat berkas **app.js**

Tulis kode berikut

```js
const express = require('express');
const simpleRouter = require('./routers/simple-router');

// Instance dari express()
const app = express();

// Mendaftarkan simpleRouter ke instance dari express()
app.use('/', simpleRouter);

module.exports = app;
```

### 6. Buat berkas **index.js**

Tulis kode berikut

```js
const app = require('./src/app');

// Port yang akan digunakan saat aplikasi jalan
const port = parseInt(process.env.PORT || '3000', 10);

// Menjalankan aplikasi Express
app.listen(port, callback);

// Fungsi yang akan dipanggil saat aplikasi telah jalan
function callback() {
    console.log('####################################################################');
    console.log('NODE_ENV   : ' + process.env.NODE_ENV);
    console.log('address    : ' + JSON.stringify(this.address()));
    console.log('####################################################################');
}
```

### 7. Buat berkas **simple-route.spec.js**

```js
const assert = require('assert');
const request = require('supertest');

// Aplikasi Express yang akan dites
const app = require('../../src/app')


describe('Simple', function () {

    describe('GET /', function () {

        it('responds with json', async function () {
            const response = await request(app)
                .get('/')       // memanggil path '/'
                .expect(200);   // cek status = 200

            assert(response.body.name, 'Ferry L. H.'); // cek di response, name = 'Ferry L. H.'
        });

    });

});
```

### 8. Ubah berkas **package.json**

Tambahkan skrip untuk menjalankan aplikasi dan tes di berkas **package.json**

```json
"scripts": {
    "start": "NODE_ENV=development node index.js",    // menjalankan aplikasi
    "test": "NODE_ENV=test mocha test/**/*.spec.js"   // menjalankan tes
  }
```

Berkas **package.json**

```json
{
  "name": "express-supertest-simple",
  "version": "1.0.0",
  "description": "Simple Express and Supertest",
  "main": "index.js",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/ferrylinton/bologhu-code.git"
  },
  "scripts": {
    "start": "NODE_ENV=development node index.js",
    "test": "NODE_ENV=test mocha test/**/*.spec.js"
  },
  "keywords": [
    "expressjs",
    "supertest",
    "mocha"
  ],
  "author": "Ferry L. H.",
  "license": "ISC",
  "dependencies": {
    "express": "^4.17.1"
  },
  "devDependencies": {
    "supertest": "^6.1.3"
  }
}
```

### 9. Jalankan tes

```shellsession
npm run test
```

```shellsession
ferry@G410:~/Documents/nodejs/bologhu-code/express/express-supertest-simple$ npm run test

> express-supertest-simple@1.0.0 test /home/ferry/Documents/nodejs/bologhu-code/express/express-supertest-simple
> NODE_ENV=test mocha test/**/*.spec.js



  Simple
    GET /
      ✓ responds with json


  1 passing (34ms)
```

## Source code

1. [Github](https://github.com/ferrylinton/bologhu-code/tree/master/express/express-supertest-simple)
2. [Gitlab](https://gitlab.com/ferrylinton/bologhu-code/-/tree/master/express/express-supertest-simple)
3. [Bitbucket](https://bitbucket.org/ferrylinton/bologhu-code/src/master/express/express-supertest-simple/)

