---
layout: post
title: "Contoh Sederhana Express dan Istanbul"
excerpt: "Contoh Sederhana Express dan Istanbul"
tags: 
 - Expressjs
 - Mocha
 - Supertest
date: 2022-01-20
author: 
  displayName: Ferry L. H.
  username: ferrylinton
svgs: 
  - node-svg
  - sequelize-color-svg

---

<pre class="line-numbers">
    <code class="language-css">
        .example {
            font-family: Georgia, Times, serif;
            color: #555;
            text-align: center;
        }
    </code>
</pre>

<pre class="line-numbers">
    <code class="language-js">
        function myFunction() {
            return true;
        }
    </code>
</pre>

<pre class="line-numbers language-markup">
    <code>
        &lt;img src="example.png"&gt;
    </code>
</pre>