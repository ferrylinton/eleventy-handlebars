const fs = require('fs');
const markdown = require('./src/config/markdown');
const transforms = require('./src/config/transforms');

const handlebars = require('handlebars');
const handlebarsHelpers = require('./src/config/handlebars-helpers');
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

let tags = null;
let authors = null;
let posts = null;

module.exports = function (config) {

  config.addWatchTarget("./src/");
  config.addWatchTarget("./eleventy.js");

  config.addPassthroughCopy({ "./src/assets/fonts": "./assets/fonts" });
  config.addPassthroughCopy({ "./src/assets/img": "./assets/img" });
  config.addPassthroughCopy({ "./src/assets/svg": "./assets/svg" });
  config.addPassthroughCopy({ "./src/assets/libs": "./assets/libs" });
  config.addPassthroughCopy({ "./src/favicon.ico": "./favicon.ico" });

  // Use handlebars and markdown
  config.setLibrary('hbs', handlebars);
  config.setLibrary('md', markdown);

  config.addPlugin(syntaxHighlight, {
    trim: true,
    lineSeparator: "\n"
  });

  // Transforms
  Object.keys(transforms).forEach((key) => {
    config.addTransform(key, transforms[key])
  })

  config.addCollection("posts", collection => {
    tags = {};
    authors = {};
    posts = [];

    const pages = [...collection.getFilteredByGlob("./src/pages/posts/**/*.*")];

    pages.forEach(page => {

      // add posts to collection.data
      posts.push({
        url: page.url,
        title: page.template.frontMatter.data.title,
        excerpt: extractExcerpt(page.template.frontMatter.content.toString()),
        date: page.template.frontMatter.data.date,
        tags: page.template.frontMatter.data.tags,
        author: page.template.frontMatter.data.author
      });

      // add tags to collection.data
      (page.template.frontMatter.data.tags || []).forEach(tag => {
        if (!tags[tag]) {
          tags[tag] = {
            tag,
            count: 1
          };
        } else {
          ++tags[tag].count;
        }

      });

      // add authors to collection.data
      if (!authors[page.template.frontMatter.data.author.username]) {
        authors[page.template.frontMatter.data.author.username] = {
          username: page.template.frontMatter.data.author.username,
          displayName: page.template.frontMatter.data.author.displayName,
          count: 1
        };
      } else {
        ++authors[page.template.frontMatter.data.author.username].count;
      }

    });
    
    return posts;
  });

  config.addCollection("tags", () => {
    return Object.values(tags);
  });

  config.addCollection("authors", () => {
    return Object.values(authors);
  });

  // Install handlebars helpers
  Object.keys(handlebarsHelpers).forEach((key) => {
    config.addHandlebarsHelper(key, handlebarsHelpers[key])
  });

  config.setBrowserSyncConfig({
    callbacks: {
      ready: function (err, bs) {

        bs.addMiddleware("*", (req, res) => {
          const content_404 = fs.readFileSync('public/404.html');
          // Add 404 http status code in request header.
          res.writeHead(404, { "Content-Type": "text/html; charset=UTF-8" });
          // Provides the 404 content without redirect.
          res.write(content_404);
          res.end();
        });
      }
    }
  });

  return {
    templateFormats: ['hbs', 'md'],
    htmlTemplateEngine: 'hbs',
    passthroughFileCopy: true,
    dir: {
      input: './src/pages',
      includes: '../includes',
      layouts: '../layouts',
      data: '../data',
      output: './public'
    }
  }
}

function extractExcerpt(content) {
  let excerpt = null;

  if (content) {
    // The start and end separators to try and match to extract the excerpt
    const separatorsList = [
      { start: '<!-- Excerpt Start -->', end: '<!-- Excerpt End -->' },
      { start: '<p>', end: '</p>' }
    ];

    separatorsList.some(separators => {
      const startPosition = content.indexOf(separators.start);

      // This end position could use "lastIndexOf" to return all the paragraphs rather than just the first
      // paragraph when matching is on "<p>" and "</p>".
      const endPosition = content.indexOf(separators.end);

      if (startPosition !== -1 && endPosition !== -1) {
        excerpt = content.substring(startPosition + separators.start.length, endPosition).trim();
        return true; // Exit out of array loop on first match
      }
    });
  }

  if (excerpt) {
    let txt = markdown.render(excerpt.trim());
    return txt.replace('<p>', '<p class="mb-1">');
  }else{
    return '<p class="mb-1">Excerpt is not found</p>';
  }
}
